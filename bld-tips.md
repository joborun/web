### [[Back to the top joborun wiki page|index.md]] ###



## Some helping hints for those building all from source
### chasing updates and upgrades from joborun obarun and arch

For those who pay attention to [https://git.disroot.org/joborun-pkg/repos/](https://git.disroot.org/joborun-pkg/repos) a few things have been added, pkg-list.diff 
There is mention in the wiki about repos and the ability to use those as repositories to monitor updates, without binaries (with the exception of pacman and mirror pkg binaries that are also available there).

```
curl https://git.disroot.org/joborun-pkg/repos/raw/branch/main/pkg-list -o pkg-list-last
```
This will save a copy of current pkg-list in whatever working directory you are in or specify it after the -o (output - save-as) 
You work and build what has been up for updates and catch-up.  In the next time you check the repos again you can do this

```
curl https://git.disroot.org/joborun-pkg/repos/raw/branch/main/pkg-list -o pkg-list-now
/usr/bin/diff -d -U 0 pkg-list-last pkg-list-now | grep -v @ | grep -v pkg-list
```
Will show you what has changed since your last run of updates.
Then move pkg-list-now to pkg-list-last till next check for upgrades.

This will show you packages removed, added, updated.
So even if sourceforge went down replacing sourceforge repositories with repos can keep you synchronized with what we upload to git, which has been 100% reliable since 2021.

### building from source in ram - tmpfs mounted chroot

Make a mount point, mount a tmpfs system (read up on man mount -t options) and explode the jobbot.tar.xz into that mount.  Use arch-chroot preferrably to chroot to it, su make, go building.
Keep in mind before you do excessive work into it, to figure a convenient way to save back up the chroot if you don't want to start from scratch after each boot.  If you use tar -cJf backup.tar.xz of the root directory make sure you have exited the chroot properly and mounts from the chroot are not there (proc,sys,dev ..) 

This is to avoid having a separate build partition.

##  To build arch packages in addition to our own

pbget from AUR we used to build but after a major revision of pacman it no longer works, you can try. xyne author and aur maintainer is/was an arch dev, the source hasn't been maintained, and it was originally a python2 script.
Here is our script for arch pkgs:
workarea  -->

```
echo pkg name
read $pkgname
mkdir -p $pkgname
cd $pkgname
echo enter file name if not PKGBUILD (default)
read file
: "${file:=PKGBUILD}"
echo $pkname $file
read
curl https://gitlab.archlinux.org/archlinux/packaging/packages/$pkname/-/raw/main/$file --no-clobber -o $file

```
You can do the same from line 5 and beyond for any support files required (patches pkgname.install configurations etc.)

Notes: 
**repos** is an alternative location of the joborun repository databases.  In pacman.conf 
use 

```
Server = https://git.disroot.org/joborun-pkg/repos/raw/branch/main/
```

After each of jobcore jobextra jobcomm and 66-EOL if you are using it
The db is the same as sf, always uploaded at the same time, just no binaries for pkgs.
So you can run -Syu and hit n for upgrading, just to get a list of available upgrades


[copy of this article in diaspora](https://diaspora-fr.org/posts/190a0520a4ef013d48f80025900e4586)
