
## scripts included in jobo-setup pkg and in our base file system image ##

#### [[Back to the top joborun wiki page|index.md]] ####


If you don't wish to [[install|https://git.disroot.org/joborun/web/src/branch/main/install2.md]] via downloading and exploding a tarball image, and follow the [[live net-installation|https://git.disroot.org/joborun/web/src/branch/main/install1.md]] method, you can find the helper scripts that make guess-work easier in jobo-setup package. 

Below see the contents on some of those scripts.

#### Install X + openbox + jwm + vtwm ####

The first script that many may be interested in is simply a file list needed on top of base to start X and openbox or jwm or vtwm.
Note here that <strong>you don't need a kernel or a fully booted system to run X</strong>, you can chroot from console from another system, and if mounting of system directories is equivalent to arch's arch-chroot, you can switch to user, go to the home directory and simply type xinit or startx.

#### installX ####
<pre>
#!/usr/bin/zsh
sudo pacman -Sy $(cat X.list) --noconfirm
# sudo /usr/local/bin/cldoc  # use if you want documentation/help removed to save space
xinit
</pre>

Also we offer an [[autologin|autologin.md]] pkg that works with runit and s6/66

cldoc is an other optional script for those who would rather reach documentation through network and don't need it in their minimalist system

#### /usr/local/bin/cldoc ####
<pre>
% sudo chmod 0755 /usr/local/bin/cldoc  
</pre>
contents:
<pre>
#!/bin/sh
sudo rm -rf /usr/share/{doc,man,html}/*
sudo rm -rf /usr/local/share/{doc,man,html}/*
</pre>

X.list is separated from the script and it is just the list of proposed packages we suggest for your first start.  Unfortunately the configurations are stored in that /home/make directory, and without them the setup will be as plain and boring as arch.

This X.list is as follows and feel free to subtract and add what you know will work better for you.  It is meant for remembering what is needed and not keep reading Xorg.0.log for what is missing.

#### ~/X.list ####

<pre>
arandr
conky
elinks
feh
jwm
leafpad
lxterminal
obconf
obmenu-generator
openbox-patched
virtualbox-guest-utils
xdg-user-dirs
xf86-video-ati
xf86-video-amdgpu
xf86-video-fbdev
xf86-video-intel
xf86-video-vesa
xf86-video-vmware
xorg-server
xorg-xinit
pcmanfm
libtool
package-query
</pre>

At this point you have to make sure you have this minimal file in your home directory:

#### ~/.xinitrc ####

<pre>
#!/usr/bin/zsh
exec openbox-session
#exec choosewm
#exec jwm
#exec vtwm
#exec lxterminal
#exec /usr/local/mlt
#exec cwm
</pre>

then simply type xinit (or startx no difference).

#### mkpkg and makepkg script ####

makepkg is the pacman subprogram for building a package from a template/recipe pacman defaults as PKGBUILD.  PKGBUILD is a <strong>bash script</strong> because pacman was written to read bash commands to build a package.  Someone should tell those fools who edit strict bash syntax and acuse predecesors of "extreme bashism", that this is what pacman was written with default, not a PKGBUILD editor's choice.  Thankfully for those fools, bash has evolved in being more elastic and accepting more universal sh syntax and not enforce strictly its own, otherwise many arch/artix/obarun/manjaro/hyperbola/parabola pkgs wouldn't build!  [[Mere linux|https://merelinux.org/]] is about the first distribution we have seen honoring this pacman quality and beginning their PKGBUILDs with a bash shebang. 


sudo chmod +x /usr/local/bin/mkpkg

<pre>
#!/bin/sh
time numactl --physcpubind=1,3,5,7 makepkg -f
</pre>

time: calculates the time interval from initiating the process till it ends, which is what the figures in "time" are in each pkg repository.

numactl: is a program that lets you control on which processors/threads to execute the program.  In this example imagine an 8core/thread machine with every other processor being used, to minimize both heat and maintain high functionality on the rest of the system.  The makepkg -f cleans up from previous run and starts building from 0.

<b>Add s6 and 66 for alternative init and supervision system.</b>

/home/make/add.66

This is how simple it is but only when the setup pre-exists from our tarball image.  To recreate the setup use the script following this one before you reboot.

<pre>
#!/usr/bin/zsh
sudo pacman -Rdd opensysusers --noconfirm
sudo pacman -S jobo66 --noconfirm
echo "enter to reboot or Ctl-C to do more before rebooting"
read
reboot 
</pre>

/home/make/66.setup
This script can be used initially or whenever you have managed to mess things up and don't know how to return to a bootable system with the initial joborun default setup.

<pre>
#!/bin/sh
sudo 66-tree -R net
sudo 66-tree -R root
sudo 66-tree -R boot
sudo 66-tree -n boot
sudo 66-tree -nE net
sudo 66-tree -ncE -S net root
sudo 66-enable -t boot -F boot@sys
sudo 66-enable -t net dhclient@eth0 dhclient@wlan0 wpa_supplicant@wlan0
sudo 66-enable -t root ntpd</pre>

you can add, and should add, sudo <b> 66-env -t boot -e nano boot@sys </b> and edit your boot module to your liking, then use enable -F boot@sys to pass the changes to the boot module.  The configuration script is pretty explanatory, but refer to the obarun wiki for details.

Also, if you are in need of user level services, dbus-session, xdg-user-dirs, pipewire/pulseaudio etc, use the boot-user-up boot-user-down scripts (also included in jobo-setup pkg).  The one issue to remember is you must exit X, exit the user, and re-enter and restart X for the user services to have a full effect on the X session.  When npt in need of user services (consolekit dbus seatd ..etc) boot-user-down should terminate them.

/home/make/rm.66

To remove s6/66 and return to your runit  only system use this rm.66 script.
Emphasis on the script to have first booted with runit before you cleanly remove s6 and 66.  The reason is that even though s6 is still pid 1 even after removing the package, it would take an existing instance of execline and skalibs to execute a proper shutdown.  s6 is nearly impossible to kill unless its own routine is followed.  You can kill every other process in the system and if s6 is pid1 it will reconstruct the system as the first moment it booted.  Try this security with any other init system, but make sure there is an sshd still running in the system before it becomes a zombie system.  "Ideal for a server" you say, you got it!

<pre>
#!/usr/bin/zsh
echo "you should ONLY remove 66 when you have booted with runit, otherwise"
echo "your system can not properly shutdown/reboot and may become corrupted"
echo "Type Ctrl-C to escape, if your pid1 is runit-init hit Enter"
read
sudo pacman -Rnsu jobo66 s6-suite boot-66serv tty@-66serv dhclient-66serv ntpd-66serv wpa_supplicant-66serv obsysusers s6-dns s6-networking --noconfirm
sudo pacman -S opensysusers --noconfirm
echo "you can now reboot and s6/66 is removed and you are back at a runit only system"
echo "you can run ~/add.66 again at any time and reboot to 66"
</pre>


### More scripts to follow: ###
<pre>
</pre>

<pre>
</pre>
