##  How repositories work in joborun and how you can add to them locally  ##

#### [[Back to the top joborun wiki page|index.md]] ####

#### Repositories ####

All our instructions are "suggestions" on how you should do it, not some rule, but if you deviate and run into problems it would take longer for us to understand what you did different to figure out why it didn't work.

If we all do things the same way we can share easier and make sure the software produced is more reliable.

When you download a package by pacman to install or upgrade, a copy is saved in /var/cache/pacman/pkg directory.  This is a default location you can change, but few people have found a reason to.

We save our own created packages in three locations:

<pre>/var/cache/jobcore
/var/cache/jobextra
/var/cache/jobcomm</pre>

So when you make a jobcore package, say coreutils, it goes into /var/cache/jobcore   

We also create /var/cache/jobcore-archive and /var/cache/jobextra-archive directories.  When a package is upgraded, before we move the new one in the jobcore repository we move the older package to jobcore-archive.
This is optional but we test each package before we send it up to the repository, in case something goes bad, we can easily revert to the previous edition.

#### Example: ####

<pre>2.5M Dec 21 18:53 /var/cache/jobcore/coreutils-9.0-02-x86_64.pkg.tar.lz
2.5M Jan 10 15:17 /var/cache/jobcore-archive/coreutils-9.0-01-x86_64.pkg.tar.lz</pre>

We changed our release numbering convention around Dec20th2021 to just adding a 0 in front of the Arch release.  So if you see arch coreutils-9.0-3 and we have coreutils-9.0-02 we need to run an upgrade and see why Arch upgraded as well.

So say you built your first 20 jobcore packages and you want to make a local repository database:

<pre>
% sudo repo-add /var/cache/jobcore/jobcore.db.tar.gz /var/cache/jobcore/*pkg.tar.lz </pre>

You can substitute in your pacman.conf our osdn mirror with your local repository and you will see you have 20 pkgs in there, the ones you made.

What happens when pacman tries to install one of those?  If first "downloads" the pkg from the repository, even though it is local.  So it really makes a copy from /var/cache/jobcore to /var/cache/pacman/pkg.  So now you have the same file twice, doubling the local space it takes.

To avoid this, do as we do:

<pre>sudo ln -s /var/cache/jobcore/*.pkg.tar.lz /var/cache/pacman/pkg/</pre>

So when pacman tries to install the package it sees already a copy in your local directory (even though it is just a link) and doesn't try to download copy from your repository.

You replicate the same logic and instructions for jobextra, just switch jobcore for jobextra everywhere.

For those who really like what we do and like to build their own, after the initial installation, and building everything from source yourself, all you need for upgrades is refreshing the .git for jobcore and jobextra.

YOU NEVER HAVE TO DOWNLOAD BINARIES AGAIN!

Except of course for all those packages we don't build yet, and you get from Obarun or Arch.  But since you are becoming more proficient you can get the PKGBUILDs for any package you want and build it locally.  

Obarun's git https://git.obarun.org/pkg  example for PKGBUILD of 66 which is in obcore:


<pre>wget https://git.obarun.org/pkg/upstream/66/-/raw/master/PKGBUILD</pre>


and you get the recipe for 66


Arch's source repo is like this:

<pre>https://gitlab.archlinux.org/archlinux/packaging/packages/arch-install-scripts/-/raw/main/PKGBUILD</pre>


You can use cower for getting AUR pkgbuilds, 

<pre>% cower -d librewolf-git  </pre>

for example and the whole trunk directory for the package is installed in the name of librewolf-git directory.  Cower is very nice and reliable, it was abandoned or finally developed, and is now adopted by Eric Vidal in Obarun.  Recnetly when Arch changed the AUR addressing scheme it needed a commit, so it is now officially an Obarun in-house software.

Another good tool for snapping pkgbuilds and the entire trunk of the source from Arch is pbget, and we build this package in jobextra.

<pre>% pbget --trunk linux-hardened </pre>

If there is testing this is the version you get.  simply pbget linux-hardened gets you the "stable" version.  If there is no testing you get the stable version either way.

It is not rocket science, is it.  Why don't so many people think of Arch as a source based distro?


