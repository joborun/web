##  joborun linux wiki home ##

#### This is the root file of the wiki set of documents, the how to, FAQ, hacks, tricks, scripts, etc, ####


| base | Runit | joborun | boot loader |
|--------------|-------|----|----|
| <a href="install.md">installation & configuration</a> | <a href="runit-setup.md">runit setup</a> | <a href="joborun-pkgs.md">jobo-setup</a> | <a href="limine.md">limine</a> 
| <a href="install2.md">installation using our tarball</a> | | |<a href="UEFI.md">UEFI boot</a> |
| <a href="howto.md">how to build with joborun</a> | | <a href="scripts.md">jobo-scripts</a>  |
| <a href="morebld.md">how to build like joborun</a> | | | <a href="pacman.md">pacman issues</a> |
| <a href="repos.md">local repository building</a> | | |<a href="autologin.md">autologin</a>  |
| <a href="jobcomm.md">jobcomm repository</a>| | |



For anything else the [[Obarun wiki|https://wiki.obarun.org/doku.php]] and [[Arch-Linux wiki|https://wiki.archlinux.org/]] should be very helpful, and our system is not that different for those wikis to be invalid, except for items relating to systemd functionality.

Help us and hold us focused to our [[goals|goals.md]] by participating, contributing, arguing (rationally), criticizing, asking dumb and smart questions, pointing out mistakes.  Make our goals OUR goals.  Of course we don't all think the same, we don't all agree about everything, but we can work towards agreements by discussing rationally our every step and every change.

Also visit our main web page at [[http://pozol.eu|https://joborun.neocities.org/joborun.html]].
