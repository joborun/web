## **How to setup an autologin service in joborun** ##



#### [[Back to the top joborun wiki page|index.md]] ####



In brief, to get it running right away, install jobcore/jobo-autologin


<pre>sudo pacman -S jobo-autologin</pre>


Then run the setup script


<pre>sudo autologin-setup</pre>



Then start the service up



<pre>sudo autologin-on</pre>




**Introduction**



Autologin most commonly, and without the complications of logind, display manager, etc. is a modification of a standard tty (console) where the sysadmin authorizes the system to be automatically accessed by a specific user in a specific tty and start her/his default cell.

Most people who use autologin also autostart a graphic session from their shell, X, wayland, or "other", so the system basically finishes stage1 booting and directly executes the user's autostart graphical session.  Startx or xinit are the most common commands in such a case, in a system like ours where a user is authorized to start an X server.   

Since rarely anyone uses as many or more than 6 ttys, we chose tty7 
as dedicated to autologin, a choice a sys admin can change if so needed.



**Services and service scripts**



Since joborun is geared up to use at least runit, optionally s6/66, and both, to maintain the freedom to boot either one, we made the service work for both systems wether they are installed or not.  One case where there may be an exception, is if in your system you only have runit, you setup and start autologin then install s6/66, you will have to at least execute 



<pre>sudo autologin-on</pre> 



one more time for the service to be enabled and start in s6/66



**Graphic autostart**


For those of you using zsh (default) enter in your ~.zlogin file what it is you want started after autologin occurs (you can add it to .bashrc if that is the shell you use).

For example:  xinit or startx or choosewm (install it first)

Then, after pressing your choice in boot loader your system will boot and then it will take you to your familiar graphic session logged in already.

For a kiosk type of system, a public library system for example, in openbox ~/.config/openbox/autostart you can add a browser with a home page of choice (library search).  All you do is press the power button and then a community member can use the system.

It is best in such systems to not have the user as a wheel group member, so the system is protected by the user executing commands that may hurt it.



**Notes on security**



Not everyon and not every system built and maintained is critical of privacy and security, some are actually intented to be public and shared by family, friends, work, community in general, and autologin cuts the redundant steps of username and password.  

If you are concerned about privacy and security of your data then autologin is not for you, immediately execute:



<pre>sudo autologin-off</pre>



and uninstall the pkg



<pre>sudo pacman -R jobo-autologin</pre>











