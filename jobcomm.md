##  Ideas and proposals on how jobcomm can work  ##

#### [[Back to the top joborun wiki page|index.md]] ####


EDIT:  July 2023  -  In the current phase of development jobcomm has been carrying all listed packages as binaries just like jobcore & jobextra.  The idea though still remains, the intention is for you to engage in building and adding your own packages instead of the lazy way of downloading and installing binaries.  In the case of browsers, other than our beloved jobextra/librewolf  it takes nearly as much time to download our binary (ie palemoon - about to be dropped because enough is enough) than to download and make the package from a 3rd party's binary (palemoon...deb).   The source/PKGBUILDs may remain in the jobcomm.git but may be removed from the pacman.db list.

So the following has been altered in practice but the underlying concern remains.  We have also shifted to match Arch's pkg list better and jobextra pkgs tend to match some in extra, while jobcomm to community.  The ones you may otherwise find in AUR and their dependencies will most likely stay in jobcomm for the deep future and be maintained as well as resources allow.

Enjoy the earlier take on the role of jobcomm and its functionality.

## jobcomm repository ##

#### 1. How it may work ####

Just like ArchLinux had the community repository for 3rd priority software, software that are neither part of the base essential system nor are they building (makedependencies) dependencies of the core system, but are very useful to many people, we are thinking of having such a similar repository.

This repository is proposed to be called jobcomm (jobcommunity on top of obcommunity and community it sounded too long) but work slightly different. 

This means that you will see the package recipe (PKGBUILD and remaining help files), you clone the repository and build the package yourself, as described in [[how to build pkgs in joborun wiki|howto.md]].

The pacman.conf entry for this would be:
** Note: Since we have switched from OSDN mirrors to Sourceforge single catch-all repository the following is replaced.

<pre>

[jobcomm]
#Server = file:///var/cache/jobcomm/
Include = /etc/pacman.d/mirrorlist-jobo
</pre>

where jobo-mirror will contain repositories such as:

<pre>
Server = http://downloads.sourceforge.net/joborun/r
</pre>


and it should be placed right below the [jobextra] entry and above Obarun's [obcore] repository.

<pre> sudo pacman -U /var/cache/jobcomm/pkgname-pkver-*pkg.tar.lz </pre>


#### 2. How jobcomm can evolve  ####

We are thinking of jobcomm more like an AUR repository rather than a coordinated joborun repository.  Sure some initial packages will be placed there, but I expect the community to add more.  Adding your maintainer's information is optional, especially a contact email so if other joborun users have an issue with the software they can contact the actual maintainer who would best be fit to answer the question.  So jobcomm in [[https://git.disroot.org/joborun-pkg/jobcomm|git.disroot.org/joborun-pkg]] jobcomm will be open to registered disroot members who want to contribute.

You can open an issue on this file and discuss this here or in reddit.com/r/joborun  

Alternatively you can fork jobcomm into 2 repositories, your own local repository and the one from osdn indicating of an update.


<pre>

[jobcomm]
Server = http://downloads.sourceforge.net/joborun/r

[jobcomm0]
Server = file:///var/cache/jobcomm0/

</pre>

The first notifies you of an upgrade the second is your local fork where you install the one you built.

<pre>
% sudo pacman -S jobcomm0/palemoon
</pre>

for example, whose pkgver and pkgrel match the one of jobcomm and only when you see a difference on jobcomm will you know it is time to upgrade, otherwise you are all caught up.
