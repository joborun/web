### [[Back to the top joborun wiki page|index.md]] ###


### How to setup runit init and services in joborun ###

    Void's and Artix wikis on runit are pretty complete and relevant, we will only focus here on our own differences.  For generaral use of runit see the links in the #References section.

### Introduction ###

    runit is a suite of tools which provides an init (PID 1) as well as daemontools-compatible process supervision framework, along with utilites which modify the organization of services and daemons.

    In our implimentation of runit we have made a few different choices to accomodate other needs.  One is the aim to make a full runit setup coexist with s6/66.  In this respect the 4 power functions (halt, poweroff, reboot, shutdown) have been moved to /usr/bin/run/ while the corresponding 66 functions have moved to /usr/bin/s6.  To correct the functionality 4 of our own power scripts have been placed in /usr/local/bin/ with the same names, and independent of what the init is (runit or s6) the system will start the correct process.  (Feb-2025 since s6/66 66-EOL are being removed, this relocation of those 4 runit init functions will soon be moved to default /usr/bin and joborun66 pkg will be removed as well)

    We have decided to leave /etc/runit/sv alone for the sys-admin to be creative.  Our bundle of service scripts has been placed in /usr/lib/runit/sv to be either linked directly to /etc/runit/runsvdir/default or copied to /etc/runit/sv where they can be customized.  Future packagin changes will not affect the /etc/runit/sv contents.  The only exception to the rules are the tty definitions that are owned by runit and runit-rc and will remain in this default location /etc/runit/sv/.

    Last, we have made no changes to the stage 1 contents and setup.  Just like artix and very similar to void's setup, anything affecting runit's stage 1 can be found in /etc/rc where 1,2,3 are defined.


.... end of note

#### To enable and start a service/daemon ####

Let us take dhcpcd as an example:


The service scripts for dhcpcd are stored in /usr/lib/runit/sv
The directory that runit parses to supervise services is /etc/runit/runsvdir/default

As root:

<pre>
% ln -s /usr/lib/runit/sv/dhcpcd /etc/runit/runsvdir/default/
</pre>

This enables and starts starts the service "under the supervision of runit's runsvdir".

To stop it and disable the same service as root:

 <pre>
 % rm /etc/runit/runsvdir/default/dhcpcd
</pre>

This is how simple things are.  You link the service scripts to the catch-all runsvdir directory and runit starts and supervises its running (one shot services are executed once per session).


#### Services and service scripts ####

    As we don't promote handholding and system administration from above, the existence of runit service scripts don't come with their corresponding dependencies, with the exception of dhclient, wpa_supplicant, ntp, and dbus, which are part of the base package.  For all other services to actually run, not only do you have to link the service file to runsvdir, you must also install the corresponding package that is run by the service manager.  In other words, if you want sddm as a display manager, the service script is there and you can enable it, but you must also install sddm itslef.

References: 

<a href="https://en.wikipedia.org/wiki/Runit">Wikipedia</a>

<a href="http://smarden.org/runit/">Runit Official Source</a>

<a href="https://wiki.artixlinux.org/Main/Runit">Artix</a>

<a href="https://docs.voidlinux.org/config/services/index.html">Void</a>

<a href="https://wiki.gentoo.org/wiki/Runit">Gentoo</a>

<a href="https://wiki.archlinux.org/title/runit">Arch</a>


