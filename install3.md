##  Install building chroot environment from any linux with pacman ##

### [[Back to the top joborun wiki page|index.md]] ###

#### ..back to installation  main menu[[section 1. |install.md]] ####


1    First create a new partition for the system to be created.  If you don't know how, see the great [[wiki.archlinux.org | https://wiki.archlinux.org/title/Installation_guide#Partition_the_disks ]] wiki on the subject.  We will only provide here information that is different than Arch Linux or Obarun.

<pre>
% cd /tmp
% wget https://git.disroot.org/joborun-pkg/jobcore/raw/branch/main/pacman/pac-inst.conf</pre>

This will get you a copy of joborun's pacman.conf file without the need for a separate mirrorlist, as it is now the default joborun setup.

For simplicity we will refer to this prepared and mounted partition as the "target" /mnt and the system from which you will make the installation will be the "host" /


###### Assumptions for this installation: ######

  -   User has partitioned, formatted, and mounted partitions <bold>on /mnt</bold>
  -   Network is functional
  -   Arguments passed to the script are valid pacman targets
  -   You are in a root shell
  -   arch-install-scripts is installed in your system (alternatively read up on the arch wiki on how to do the mounting of filesystems into the target before you use the common chroot - proc/ run/ sys/ dev/ )

One of the key differences with Arch is that joborun and Obarun have a few more repositories that are placed higher up in hierarchy within /etc/pacman.conf to give priority to joborun's packages over Obarun, and Obarun's packages over arch.  (Obarun's packages other than s6/66 are generally rebuilds without systemd/libs running dependencies.  Joborun's packages over Arch are rebuilds without systemd/libs makedependencies and presence).

#####  Create pacman lib directory in /mnt #####

<pre>
# mkdir -p /mnt/var/lib/pacman
</pre>

This is necessary for installing pacman to the target in order to use the current and correct joborun pacman.conf.

##### install joborun's pacman to target #####

<pre>
# pacman -Sy pacman --config /tmp/pac-inst.conf -r /mnt
# pacman -S jobbot --config /tmp/pac-inst.conf -r /mnt
</pre> 

**   Sysusers should be used in lack of systemd to set user/group IDs for special system users like systemd does.  With the use of s6 and 66 obsysusers is a dependency and replaces opensysusers.  Basically it is the same script  and runs through a pacman hook reading a sysusers.d directory for users settings by any software that require to execute something as a special system user (neither a regular user or root).   On the buildbot we have neither runit or s6 nor 66, nothing points to a bootable system, but some software still need sysusers to install properly.

Select the closest mirrors to your location and remove the comment sign '#' from in front of them.  Pacman hits the first uncommented server on the list and only if it fails goes to the next.  You do this by editing: 
/etc/pacman.d/mirrorlist and 
/etc/pacman.d/mirrorlist-jobo

This is a screened list from Arch to only include https:// mirrors, so some countries' mirrors that only offer an http:// service have been ommitted.  You can always replace our https only mirrorlist with mirrorlist-arch which includes the http: only sites (an exact copy of the current arch mirrorlist).

<pre>
# $EDITOR /mnt/etc/pacman.d/mirrorlist
</pre>

Initialize pacman, re-populate its gpg-keys and update the database

<pre>
# arch-chroot /mnt pacman-key --init
# arch-chroot /mnt pacman-key --populate joborun archlinux obarun
# arch-chroot /mnt pacman -Syu
</pre>

Select the lingual group of your installation by removing the '#' (uncommenting - example: "en_US.UTF-8 UTF-8"
for US English ) and pass the choice into the system:

<pre>
# $EDITOR /mnt/etc/locale.gen
</pre>

<pre>
# arch-chroot /mnt locale-gen
</pre>

If you are working in chroot don't expect the change in locale to be automatic, you must either restart a shell, or exit and re-enter.


##### Your installation is complete #####

Use arch-chroot to get into the installation and do more configurations or pkg installations as you need.

### **Welcome to joborun build-environment** ###


What you will need now is a user, the standard user in the jobbot image is make and the home directory for this user is /home/make  If you choose another keep in mind that for simplicity "make" will be referred to as the standard user in other documentation.

useradd make (consult the arch wiki https://wiki.archlinux.org/title/users_and_groups) for options and specific flags or the useradd -h menu

In trying to keep jobbot as minimal as possible man-pages do not work without the man-db/man-pages pkgs.  If you need to read them for the installed packages install those and remember to uninstall them when you are done.  If not, when you run ckchroot command at the end or beggining of building it will show you that you have additional packages in the environment.

### ** Switch to /src/pkg and  clone our repositories ** ###

- Install git
- Use the commands listed in the README stored in /src/pkg to clone the repositories.  If youjust want to refresh them use pull instead of clone
- Uninstall git
- Run /usr/local/bin/ckchroot to verify your environment ONLY has what is prescribed to be the minimal building environment, nothing more or less.

Go build some packages (read our wiki entries for building and building like joborun)


#### Note: ####
Thanks to [[u/unix21311 | https://www.reddit.com/user/unix21311/]] a detail that escaped us, not actually using a different distro to do the installation, till pacman and mirrorlists are installed in the chroot the flag --config /mnt/etc/pacman.conf must be used for your local pacman to be reading a different configuration and repositories than what is native.  So the 2nd step of installation was modified.  In the first step you use the pacman.conf that is temporary to install joborun's pacman, the 2nd step is using the chroot's own new pacman.conf.

<pre>pacman -Syu -r /mnt</pre>

uses the native (host's) pacman to install into the chroot /mnt


<pre>pacman -Syu --config /tnp/alternative-pacman.conf -r /mnt</pre>

uses this alternative pacman.conf stored somewhere else


<pre>sudo arch-chroot /mnt pacman -Syu</pre>

uses the pacman installed into  /mnt (target) and therefore /mnt/etc/pacman.conf



