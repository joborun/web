### [[Back to the top joborun wiki page|index.md]] ###

**Goals of joborun-linux**

Stay as compatible to Arch-Linux and packaging as possible

Build everything that makes a minimal base system and base devel tools for building software in the absence of systemd and its related libraries

Provide the user/sys-admin an easy way to build and upgrade their chosen software from source instead of relying on binary blobs built in possibly incompatible systems.

Suggest a more reasonable working graphic environment that depends less on automation and control of strange polkit, logind, dbus intercommunication, automounting and dismounting.  Use a leaner more controllable and secure system.

Compliment Obarun's effort to provide a state of the art init and service supervision system without systmed/libs running dependencies, therefore releave its burden of having to build software based on Arch tools infested by systemd.

Maintain the cutting/bleeding edge contact with all upstream sources of software by following Arch-Linux testing repositories.

Discard suspiciously complex mega-corporation tools recently adopted, such as zstd (facebook's FOSS compression/decompression software), obarcle's zfs (also incorporating internally facebook's zstd), and building tools such as google's gtest (used for building zstd!) and return to good trusty alternatives such as xz, gz, bzip2, etc.

Easily reconfigurable and currently tested with 0 ill effects, why have we incorporated a solution to other peoples' problems by adding ipv6 complexity to where it doesn't belong?  Simply the internet is not "ours", it is "theirs", a handful of corporations and a few government agencies control its entirety.  Ipv6 came to solve "their" problem, not ours.  When it becomes ours we will deal with it.  So to test this, we have turned ipv6 off from kernel to x11.  You'll be surprised how many pieces of your software have direct ipv6 connectivity!  The only few sites that can't be reached are ipv6 test sites that tell you you have ipv6 connectivity.  We can't reach those, and they can't reach us!

Provide a working solution of how to compare two or more init and service supervision/management systems by making runit and s6/66 coexist in the same system, and the choice to boot and run either one.  Encourage therefore the so called init freedom not in the way artix has forced the split choice but simply altering the bootloader's command.

Eventually supplement all other repositories with our own (if Obarun declines to cooperate and join the effort), still use pacman and follow Arch-Linux development but rely on solely source built software.  This goal will depend on the collaborative effort of community members/joborun sys-admins.

Finally, when the conditions are right, and under collective decision, to parallely build the same system based on the musl C-library instead of the traditional GlibC, due to its size, cleaningness, and speed.  If Void can do it, why not have an ARCH-musl flavor?

Help us redefine goals, principles, and direction by contributing both ideas and work.  Ideas are cheap without commitment to contribute work.  This project will only go as far as its involved community will like to take it.

If Obarun was to resemble a road legal Porsche sportscar, joborun is more of a factory developed world championship race car.  Expect nothing less or nothing more.

**It is so good it should be banned**