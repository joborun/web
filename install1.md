##  From any linux with pacman installed install joborun from terminal ##

### [[Back to the top joborun wiki page|index.md]] ###

#### ..back to installation  main menu[[section 1. |install.md]] ####


1    First create a new partition for the system to be created.  If you don't know how, see the great [[wiki.archlinux.org | https://wiki.archlinux.org/title/Installation_guide#Partition_the_disks ]] wiki on the subject.  We will only provide here information that is different than Arch Linux or Obarun.

<pre>
% cd /tmp
% wget https://git.disroot.org/joborun-pkg/jobcore/raw/branch/main/pacman/pac-inst.conf</pre>

This will get you a copy of joborun's pacman.conf file without the need for a separate mirrorlist, as it is now the default joborun setup.

For simplicity we will refer to this prepared and mounted partition as the "target" /mnt and the arch system from which you will make the installation will be the "host" /

For EFI systems also follow the wiki entry [[UEFI|UEFI.md]] or the arch wiki [[EFI system partition|https://wiki.archlinux.org/index.php/EFI_system_partition]].

###### Assumptions for this installation: ######

  -   User has partitioned, formatted, and mounted partitions <bold>on /mnt</bold>
  -   Network is functional
  -   Arguments passed to the script are valid pacman targets
  -   A valid mirror appears in /etc/pacman.d/mirrorlist
  -   You are in a root shell
  -   arch-install-scripts is installed in your system (includes arch-chroot & genfstab which will be used here)

One of the key differences with Arch is that joborun and Obarun have a few more repositories that are placed higher up in hierarchy within /etc/pacman.conf to give priority to joborun's packages over Obarun, and Obarun's packages over arch.  (Obarun's packages other than s6/66 are generally rebuilds without systemd/libs running dependencies.  Joborun's packages over Arch are rebuilds without systemd/libs makedependencies).

######  Create pacman lib directory in /mnt ######

<pre>
# mkdir -p /mnt/var/lib/pacman/sync
</pre>

This is necessary for installing pacman to the target in order to use the current and correct joborun pacman.conf.

###### install joborun's pacman to target ######


<pre>
# pacman -Sy pacman --config /tmp/pac-inst.conf -r /mnt
# pacman -S base --config /tmp/pac-inst.conf -r /mnt
# /mnt/usr/bin/arch-chroot /mnt
</pre> 


If you want additional packages you can either do this later through chroot or add them at the end of the above command (ie networking package of choice, text editor, arch-install-scripts, bootloader-grub/syslinux, etc. ).
We are trying to make sure you have a fully functional system to boot and login to console, then you can install whatever you like.

Select the closest mirrors to your location and remove the comment sign '#' from in front of them.  Pacman hits the first uncommented server on the list and only if it fails goes to the next.  You do this by editing: 
/etc/pacman.d/mirrorlist and 
/etc/pacman.d/mirrorlist-jobo

This is a screened list from Arch to only include https:// mirrors, so some countries' mirrors that only offer an http:// service have been ommitted.  You can always replace our https only mirrorlist with mirrorlist-arch which includes the http: only sites (an exact copy of the current arch mirrorlist).

The following is about the same as running joborun-setup from (jobo-setup) pkg.  Install jobo-setup and execute the joborun-setup script to take care of the rest of the configuration.  Alternatively take care of the following:

<pre>
# $EDITOR /mnt/etc/pacman.d/mirrorlist
</pre>

Initialize pacman, re-populate its gpg-keys and update the database

<pre>
# /mnt/usr/bin/arch-chroot /mnt # (zsh or bash not specifying defaults to bash)
# pacman-key --init
# pacman-key --populate joborun archlinux obarun
# pacman -Syu
# pacman -S linux*** grub 
# joborun-setup
# exit
# reboot
</pre> 

***  You have a choice of joborun's special linux-lts (5.10) or linux (5.15) and if you need linux/linux-lts headers add them to the list.  Same for linux-firmware, especially if you have an AMD machine with Radeon gfx card, you will not get anything on screen without it.  There are also less frequently maintained kernel's 4.9 5.4 6.6 (current arch linux-lts but with our modifications) ... 

We highly recommend linux-lts (5.10) as a very well developed kernel expiring in 2026, while 5.15 is expected to last only till 2024. Current late lts is 6.6 carried by Arch while 6.9 is current stable.  So the choices are wide.

To install an arch kernel, for example current 6.9 labeled as linux:

<pre>pacman -S core/linux{,-headers}</pre>

will install linux and linux-headers from arch core repository and will replace our linux 5.15 (headers are optional for those that need them)

_______________________________________________

The following are minimal configurations, if you run joborun-setup you will be guided doing the same and not forget any important steps, like a password for the user to be able to login to the system once booted.

Select the lingual group of your installation by removing the '#' (uncommenting - example: "en_US.UTF-8 UTF-8"
for US English ) and pass the choice into the system:

<pre>
# $EDITOR /mnt/etc/locale.gen
</pre>

<pre>
# /mnt/usr/bin/arch-chroot /mnt locale-gen
</pre>

If you are working in chroot don't expect the change in locale to be automatic, you must either restart a shell, or exit and re-enter, or in a booted system relogin.  By reboot it will for sure change the language.

Create an fstab entry of your new installation:

<pre>
# /mnt/usr/bin/arch-chroot /mnt
# genfstab -U /mnt >>/mnt/etc/fstab
# exit
</pre>

Create a root password

<pre>
# /mnt/usr/bin/arch-chroot /mnt passwd
</pre>

edit your hosts as per your network needs 

<pre>
# $EDITOR /mnt/etc/hosts
</pre>

edit your hostname, although it should also be set by configuring your boot module in 66 or editing /etc/rc.conf for runit.

<pre>
# $EDITOR /mnt/etc/hostname
# $EDITOR /mnt/etc/rc.conf
</pre>


###### 7 Misc. optional configurations ######

Install and set your boot loader as per Obarun and/or arch wikis if necessary:

[[grub|https://wiki.obarun.org/doku.php?id=grub]]  [[syslinux|https://wiki.obarun.org/doku.php?id=syslinux]] [[lilo|https://wiki.archlinux.org/title/LILO]]  [[UEFI|UEFI.md]]

We have also built the [[limine|https://wiki.archlinux.org/title/Limine]] boot loader software, long before it appeared on AUR and later extra, which is simple and one configuration works on both MBR/Bios booting and GPT/UEFI boot.  Unfortunately it will not work with btrfs or xfs yet, so use grub if you choose those filesystems.  Correction, late versions do not work with ext2,3,4 either, no longer supported, so the developers of limine have shifted in only supporting fat32 in /efi /boot which now they demand you have, even on dos/MBR partitioning table.  We find this very anti-unix like perception and we will no longer carry their package.  Feel free to get it from Arch in the future and do as you wish. (10/19/2023)

[[#Note|note]]


###### Your installation is complete ######

You may either reboot at this point or use arch-chroot to get into the installation and do more configurations or pkg installations as you need.

## **Welcome to joborun** ##


#### Note: #### In general, the hierarchy as seen in pacman.conf works in reverse for wiki.  What you can not find here you may find in Obarun, as it applies here as well.  What you can't find in Obarun wiki you can find in wiki.archlinux.org, and it applies for Obarun, and applies here as well.  With the exception of anything that relates to systemd/logind.  On the other side of the coin though there may be a tendency to overbrowse Artix wiki, and with the very specific runit similarity there is nothing that should be relevant, compatible, and non-destructive.


<pre>pacman -Syu -r /mnt</pre>

uses the native (host's) pacman and mirrorlist to install into the chroot /mnt

<pre>pacman -Syu --config /tnp/alternative-pacman.conf -r /mnt</pre>

uses this alternative pacman.conf stored somewhere else

<pre>/mnt/usr/bin/arch-chroot /mnt pacman -Syu</pre>

is a much better and safer way to do an upgrade of the system than the previous command, all because some hooks on pacman pkgs need a process based on the tmpfs systems that are only available through the proper chroot the arch-chroot script provides.



