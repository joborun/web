
### [[Back to the top joborun wiki page|index.md]] ###

#### ..nack to installation  main menu[[section 1. |install.md]] ####



As Arch-wiki entries are some times too theoretical and offer little practical solution assistance, and since booting uefi systems has caused headaches to users in most linux installations, we dedicate this section as a supplement to the Arch entry.  The article is based on a contribution by marianarlt, who has developed a high level of expertise in setting up the bootloader for various UEFI capable or non-Bios systems.

UEFI booting Joborun

  Disclaimer

<p>As with most manuals you should first <b>entire document with attention</b> and then apply the contents. <strong>When installing Joborun to boot with UEFI make sure you have originally booted a live medium through EFI as well.</strong>.<sup><a href=#foot-1>[1]</a></sup></p>

  Requirements

<ul>
  <li>The disk you want to install to should have a GPT table. <i>(gdisk or gparted are recommended)</i></li>
  <li>The disk will need a separate EFI system partition to boot from with the appropiate identifier bit (flag) set. <i>(again, gdisk recommended)</i></li>
  <li>A system root partition for your operating system to reside on.</li>
</ul>

  Optional Installation Steps

<ul>
  <li>A swap partition may be sometimes needed to fully support Standby/Sleep/Hibernation functions.<sup><a href=https://wiki.archlinux.org/index.php/Power_management/Suspend_and_hibernate>[Arch Wiki]</a></sup></li>
  <li>A separate partition for your users directories is often recommended as it detaches system upgrades/migration and similar activites from your personal data.</li>
  <li>Encryption <em>(not covered by this manual)</em><sup><a href=https://wiki.archlinux.org/index.php/Disk_encryption>[Arch Wiki]</a></sup></li>
</ul>

​    About Partitioning

<p>Before installing <code>Joborun</code> you want to make sure the above requirements are satisfied and you made up your mind about any optional steps. Your installation disk is most probably going to be <b>sda</b> but you should make sure with <code>`lsblk`.</code><br><br>
  The <b><i>most minimal</i></b> layout for Joborun on UEFI could be as follows:</p>
<table style="padding:0 1.5em">
  <tr style="text-align: left">
    <th>Partition</th>
    <th>Type</th>
    <th>Size</th>
    <th>Mount</th>
    <th>File System</th>
  </tr>
  <tr>
    <td>/dev/sda1</td>
    <td>esp</td>
    <td>550MB+<sup><a href=#foot-2>[2]</a></sup></td>
    <td>/efi<sup><a href=#foot-3>[3]</a></sup></td>
    <td>Fat32<sup><a href=#foot-4>[4]</a></sup></td>
  </tr>
  <tr>
    <td>/dev/sda2</td>
    <td>root</td>
    <td>Rest</td>
    <td>/</td>
    <td>Ext4</td>
  </tr>
</table>




<p>More realistically you may want to do something similar to the following though:</p>

<table style="padding:0 1.5em">
  <tr style="text-align: left">
    <th>Partition</th>
    <th>Type</th>
    <th>Size</th>
    <th>Mount</th>
    <th>File System</th>
  </tr>
  <tr>
    <td>/dev/sda1</td>
    <td>esp</td>
    <td>550MB+<sup><a href=#foot-2>[2]</a></sup></td>
    <td>/efi<sup><a href=#foot-3>[3]</a></sup></td>
    <td>Fat32<sup><a href=#foot-4>[4]</a></sup></td>
  </tr>
  <tr>
    <td>/dev/sda2</td>
    <td>swap</td>
    <td>2GB±<sup><a href=#foot-5>[5]</a></sup></td>
    <td></td>
    <td>Swap</td>
  </tr>
  <tr>
    <td>/dev/sda3</td>
    <td>root</td>
    <td>30GB±<sup><a href=#foot-6>[6]</a></sup></td>
    <td>/</td>
    <td>Ext4</td>
  </tr>
  <tr>
    <td>/dev/sda4</td>
    <td>home</td>
    <td>Rest</td>
    <td>/home</td>
    <td>Ext4</td>
  </tr>
</table>

<p>Your actual needs may differ and layouts and formats may be changed to accomodate those needs as mentioned before. This example is a tried and safe, easy to configure and maintain, layout. <b>The following instructions will therefore apply to this example.</b> Please inform yourself well when choosing different file systems and layouts, including encryption.</p>
​    Step by Step

<ol>
  <li style="margin-bottom:2em"><b>Log in to the live media</b> shell with user <code>`oblive`</code> and password <code>`toor`</code></li>
  <li style="margin-bottom:2em"><b>Set up your disk layout</b> <i>(preferably with gdisk)</i>. <strong>Make sure you choose the right disk!</strong> <i>(`lsblk` when in doubt!)</i> In this example:
    <ol style="margin-top:2em">
      <li><code>`sudo gdisk /dev/sda`</code></li>
      <li><b>Create a new GPT table</b> by choosing <code>`o`</code>.</li>
      <li>Confirm with <code>`y`</code>.</li>
      <li><b>Create a new ESP partition</b> by choosing <code>`n`</code>.</li>
      <li>Confirm the default starting sector by pressing <code>ENTER</code> then for the ending sector type <code>`+550M`</code> and confirm.</li>
      <li>You will automatically be prompted for the partition type identifier. This will be your EFI system partition so type <code>`<b>ef00</b>`</code> and confirm.<br><b>Note:</b> The identifier is <i>not</i> optional! The EFI system partition needs to be identified as such by the motherboard!</li>
      <li><b>Create a new swap partition</b> by choosing <code>`n`</code>.</li>
      <li>Confirm the default starting sector by pressing <code>ENTER</code> then for the ending sector type <code>`+2G`</code> and confirm.<br>This will be your swap partition so type <code>`8200`</code> and confirm when prompted for the identifier.</li>
      <li><b>Create a new root partition</b> with <code>`n`</code> and by now you get the idea, make it the size you want for your root partition.<br>Give it the identifier <code>`8304`</code> which stands for: Linux&nbsp;x86&#8209;64&nbsp;root&nbsp;(/).</li>
      <li><b>Create your home partition</b> in the same way and give it the identifier <code>`8302`</code> which stands for: Linux&nbsp;/home.</li>
      <li><b>Write your changes</b> out with <code>`w`</code>. You will exit out of gdisk.</li>
    </ol>
  </li>
  <li><b>Format the partitions</b> accordingly. You can <strong>use labels to make further reference easier</strong> <i>(highly recommended; useful for fstab/EFISTUB etc.)</i> In this example:
    <ol style="margin-top:2em">
      <li><code>`sudo mkfs.vfat -n ESP /dev/sda1`</code></li>
      <li><code>`sudo mkswap -L SWAP /dev/sda2`</code></li>
      <li><code>`sudo mkfs.ext4 -L ROOT /dev/sda3`</code></li>
      <li><code>`sudo mkfs.ext4 -L HOME /dev/sda4`</code></li>
    </ol>
  </li>
  <li style="margin-top:2em"><b>Mount the partitions</b> to prepare for the installation:
    <ol style="margin-top:2em">
      <li><code>`sudo mount /dev/sda3 /mnt`</code></li>
      <li>(a) <b>If</b> you want to boot with <strong>EFISTUB</strong>:<sup>(see below)</sup><br><code>`sudo mkdir /mnt/boot && mount /dev/sda1 /mnt/boot`</code><br>(b) <b>If</b> you want to boot with <strong>GRUB</strong>:<sup>(see below)</sup><br><code>`sudo mkdir /mnt/efi && mount /dev/sda1 /mnt/efi`</code><sup><a href=#foot-3>[3]</a></sup></li>
      <li><code>`sudo mount /dev/sda4 /mnt/home`</code></li>
      <li><code>`sudo swapon /dev/sda2`</code></li>
    </ol>
  </li>
  <li style="margin-top:2em"><b>efivarfs and sysfs-efivars</b>
    <ol style="margin-top:2em">
      <li>Consult this section of this document for details: <a href="https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface#Inconsistency_between_efivarfs_and_sysfs-efivars">Inconsistency_between_efivarfs_and_sysfs-efivars</a> if installation is not done from a non-efi system, or arch-chroot is not used.</li>
<li>    Check if efivarfs is mounted in /sys/firmware/efi </li>
<li>    If efivarfs is not automatically mounted at /sys/firmware/efi/efivars during boot, then you need to manually mount it to expose UEFI variables to userspace tools like efibootmgr: <br>
<code>    # mount -t efivarfs efivarfs /sys/firmware/efi/efivars </code><br>
  <i>    Note: The above command should be run both outside (before) and inside the chroot, if arch-chroot is not used.</i><br>
  Also consult your local documentation on efivar: <code>man efivar</code></li>
    </ol>
  </li>
  <li style="margin-top:2em">Make sure you <b>have a network connection established.</b><sup><a href=https://wiki.archlinux.org/index.php/Network_configuration>[Arch Wiki]</a></sup></li>
  <li style="margin-top:2em"><strong>Run <code>`sudo Joborun-install`</code></strong>
    <ol style="margin-top:2em">
      <li>Update the installer and its themes by choosing <code>`yes`</code> when prompted.</li>
      <li>Customize the installer to your needs as described in the <a href=/to/section>"Installation" section</a> of this Wiki.</li>
      <li><strong>When prompted for a bootloader choose <code>`No`</code></strong></li>
      <li>Finish the installation</li>
      <li>Optional: If you want to install microcode this is a good moment to do so. Choose <code>`Launch a shell on /mnt (7)`</code>. This will chroot you into your install and let you install additional packages.</li>
      <li>Exit out of the script</li>
      <li><b>Do not reboot yet!</b><br>(Your motherboard doesn't know what EFI application to boot yet!)<sup><a href=#foot-7>[7]</a></sup></li>
    </ol>
  </li>
  <li style="margin-top:1em">Choose your bootloader</li>
</ol>
<h4>A Word About Bootloaders</h4>
<p>For the sake of simplicity it is, for now, discouraged to use syslinux for booting layouts other than BIOS/MBR.<sup><a href=#foot-8>[8]</a></sup></p>
<p>This manual provides two easy to set up options for booting a UEFI/GPT scheme for you to choose from.</p>
<ul>
  <li><strong>EFISTUB</strong> <sup><a href=https://wiki.archlinux.org/index.php/EFISTUB>[Arch Wiki]</a></sup><br>
    This is just your motherboads integrated UEFI bootloader. That is right. Your UEFI motherboard already got a pretty sophisticated bootloader integrated. Why not just use it?
  <br>It makes for a minimalistic and direct boot approach. You configure it to boot right off vmlinuz-linux which is the default Arch Linux EFI application.<br>Some people find it difficult to set up and maintain but it actually requires less configuration than most bootloaders and just as much when changing your boot entry.<br>
  Some EFI implementations, especially early ones, might behave weird when trying to control the UEFI boot menu.
  </li>
  <li style="margin-top:2em"><strong>GRUB</strong> <sup><a href=https://wiki.archlinux.org/index.php/GRUB#UEFI_systems>[Arch Wiki]</a></sup>
    <br>The well known major bootloader of a many Linux distributions. Now that you know that your UEFI already <i>is</i> a bootloader you should understand that installing a dedicated bootloader software to your operating system is actually superfluous.<br>
    That is because UEFI will now boot off the GRUB EFI application which in return will chain towards the default Arch EFI application vmlinuz-linux.<br>
    Some people find this easier to control though and to set up with multi boot configurations.
  </li>
</ul>
<p style="margin-top:2em">Now that you know about both it is up to you to decide. They are both easy to set up.</p>

<h4>Configure UEFI Boot with EFISTUB</h4>
<ol>
  <li><code>`sudo efibootmgr -c -d /dev/sda -p 1 -L "Joborun" -l \vmlinuz-linux -u "root=LABEL=ROOT ro resume=LABEL=SWAP quiet initrd=\intel-ucode.img initrd=\initramfs-linux.img"`</code><br>
    (Any custom kernel parameters go between the brackets of the -u option as demonstrated with the `quiet` parameter. Adjust the microcode to your architecture or delete it from the command.)
  </li>
  <li style="margin-top:2em">Reboot & Enjoy!</li>
  <p>From here on whenever you want to change the boot entry you would want to delete the existing one with <code>`sudo efibootmgr -b 0000 -B`</code> where <code>0000</code> must match your Joborun entry and then recreate it with the above command. Refer to the provided links for more details.</p>
</ol>



 Configure UEFI Boot with GRUB 


  Conclusion

<p>As you see the complicated part with bootloaders in general is the system setup itself. Instructing the bootloader can be a one liner in an UEFI configuration. The Joborun team hopes that this was of help to you. Further information and discussion can be found in <a href=https://www.reddit.com/r/joborun>the Joborun Forum</a>.</p>



 Foot Notes

<p id="foot-1"><small>[1] To boot your live media in UEFI mode go into your motherboards boot menu after pressing the power button on your PC. This is specific to your motherboard and often shows briefly on the first screen that appears after pressing the power button. Most times it is one of the function Key F1 or F12 (hp is F9) key. Inside your motherboard's boot menu choose to run your live medium with UEFI. The entry will literally say "UEFI" and the device name (most probably disk name or a USB flash drive).
If after booting your live medium you're still unsure you may issue <code>`ls -lh /sys/firmware/efi/efivars`</code>. If booted in UEFI mode this command will print a lot of names with long numbers, with BIOD/legacy booting it will print nothing.</small></p>
<p id="foot-2"><small>[2] Don't go smaller on the size for your EFI system partition. It is unnecessary with todays disk sizes and may cause confusion and bugs in older EFI implementations. To learn more about EFI/UEFI in general refer to the very in depth and excellent articles written by Rod Smith, the creator of rEFInd: <a href=https://www.rodsbooks.com/efi-bootloaders/principles.html>https://www.rodsbooks.com/efi-bootloaders/principles.html</a></small></p>
<p id="foot-3"><small>[3] Arch is deprecating the mount point /boot/efi for the ESP in favor of /efi. See <a href=https://wiki.archlinux.org/index.php/EFI_system_partition#Mount_the_partition>https://wiki.archlinux.org/index.php/EFI_system_partition#Mount_the_partition</a>.<br>
    It is important to realize that the actual mount point of the ESP does not matter in terms of how the UEFI functions. The ESP will always be searched by the UEFI no matter the mount point, for the EFI application to boot from. The only importance of the mount point is when configuring the bootloader.
    The mount point of the GRUB EFI application is passed to the NVRAM entry. That then gets booted by the UEFI which then in return chain loads itself into the vmlinuz-linux EFI application.
    EFISTUB uses /boot for convenience as the EFI bootloader will get configured to directly boot the vmlinuz-linux EFI application from that default location.</small></p>
<p id="foot-4"><small>[4] The UEFI specification mandates support for the FAT12, FAT16, and FAT32 file systems (<a href=https://uefi.org/specifications>see UEFI specification version 2.8, section 13.3.1.1</a>). Still any conformant vendor can optionally add support for additional file systems as is for example, the firmware in Apple Macs which supports the HFS+ file system, and newer MS-windows using NTFS as their efi partition.</small></p>
<p id="foot-5"><small>[5] Try to compensate your memory for RAM intensive tasks. If you know you do a lot of memory intensive tasks which require more RAM than you have installed then raise this.  Between 50% and 100% of Ram size is a common size.  Remember that Arch based Linux distros like Joborun need a swap for hibernation.</small></p>
<p id="foot-6"><small>[6] When sizing the root partition with a separate home partition think about future system upgrades, huge applications and other things you might install on the road. Don't be too shabby here.</small></p>
<p id="foot-7"><small>[7] Technically that is not correct. By specification the UEFI will search anywhere inside of the EFI system partition for an EFI application called BOOTX64.EFI for 64-bit systems and BOOTIA32.EFI for 32-bit systems. GRUB can even be instructed to install this way which would make NVRAM entries unnecessary.</small></p>
<p id="foot-8"><small>[8] That does not mean you can not boot UEFI/GPT with syslinux. However it still has some limitations as of 2019 and requires some expertise which would imply more overall instructions to this manual. Refer to the Arch Wiki for details. <a href=https://wiki.archlinux.org/index.php/syslinux#UEFI_Systems>https://wiki.archlinux.org/index.php/syslinux#UEFI_Systems</a></small></p>



