#### [[Back to the top joborun wiki page|index.md]] ####

## How to optimize building for dedicated use to a single class machine/s ##

### Build optimization for speed makepkg.conf ###

[[gentoo guide to optimized building | https://wiki.gentoo.org/wiki/GCC_optimization]]

If you are building from source for use on one machine (or type of machines with same class cpu) you can use GCC flags to optimize building for this machine. The best reference on how to do so comes from gentoo (no surprise there).

[[ arch wiki about makepkg flags | https://wiki.archlinux.org/title/Makepkg]]

See how and where to apply this tuning on the above link of the arch wiki

Also carefully read documentation and use jobcomm/makepkg-optimize added in our jobcomm repository.

