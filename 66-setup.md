### [[Back to the top joborun wiki page|index.md]] ###


edit 6/24:  All the 66 instructions here apply to the older 66 before 0.7.0.0 edition still available together with appropriate s6/skalibs dependencies as a group 66-EOL.  So install 66-EOL.  For newer 66 you have to enable obarun's obcore and observice repositories in pacman.conf and install them directly.  This will not affect runit, it is still able to boot as long as init=/usr/bin/runit-init is added to the boot linux line.


edit:  Note that on the joborun image available (see the download page on the main web page at http://pozol.eu) /usr/local/bin/ contains a script for adding 66 to the existing runit installation and contains an intial setup.
If after playing with it you think you messed up, run this other script called 66.setup, which pretty much does what is explained here.  This will return you to the default 66 bootable state.

Speaking of 66 setup there is also a pair of scripts 66-user-up/down for setting up user service trees.  We will explain this in a different document.

#### How to setup 66 trees and services in joborun ####


Do not be afraid of s6/66, they are your best friend.  Experiment all you like, and if something goes wrong, know that it is really simple to get back to a bootable functional system.

### this is how ###

We can only use our own default setup as a suggestion, but if and when you make different choices (than our boring simplicity) it is simple to record your setup and store it so you remember what you have done.

#### Are you happy with how everything works now, or the initial Joborun setup? ####

If yes, save it!

How?    (as root is indicated always with a # sign as user will be % )

<pre># 66-intree -zg </pre>

Shows you your system trees, the services enabled in each one, and their current status and logs.

<pre># 66-intree -g >/etc/66/mytrees.txt</pre>

Will save your tree structure in this text file.  You can use it later if you run into problems to revert to it.  Let's assume this is the same as stored in the tarball image of joborun.

<pre>
# 66-intree -d0 | grep Name
</pre>

This will give you a list of the names of all your trees (root level but same command as user will give the names of user trees).

<pre>
# 66-tree -R boot
# 66-tree -R net
# 66-tree -R root
</pre>

This will remove the existing trees.

<pre>
# 66-tree -n boot
# 66-tree -nE net
# 66-tree -ncE -S net boot
</pre>

This will recreate those three trees which are now blank.  Tree boot is the default boot tree as defined in /etc/66/init.conf, doesn't need to be enabled, as it is by default.  Tree net is enabled and it is where network services should be so you have network access as soon as boot finished (similar to stage 2 in runit).  Tree root is both enabled, and current/default for any other service you want to enable.  Tree root starts right after net.  

<pre># 66-intree -zg </pre>

This will verify those trees exist and are empty (no services enabled).  But it is an optional check.

<pre>
# 66-enable -t boot boot@sys
</pre>

This enables the boot module boot@sys in tree boot, it is a name we choose for module boot@.  You can substitute sys for any name but you must use one, even if it is a single character, like boot@z

<pre>
# 66-enable -t net dhclient@eth0 dhclient@wlan0 wpa_supplicant@wlan0
</pre>

This enables those three services in tree net.

<pre>
# 66-enable -t root ntpd
</pre>

This enables the time service ntpd on tree root.

Now, you most likely need to reconfigure the default boot@sys module, replace your hostname, choose the number of ttys you want active at boot, your bios clock and timezone, among other parameters.  To do so:

<pre>
# 66-env -t boot -e nano boot@sys
</pre>

If you don't like using nano but another console text editor replace nano with the one of choice.  It is a self explanatory configuration file.  Make sure when you turn options on that you do have the necessary software for those options to take effect.  For further details see the documentation at www.obarun.org.

<pre>
# 66-enable -t boot -F boot@sys
</pre>

To pass the changes of the boot module to the system for the next reboot you must enter the above command.  It **F**orces the replacement of the configuration.

Now see what you have created, it should look just like the setup when you first installed and booted joborun.

<pre># 66-intree -zg </pre>

Compare the output with had you had initially saved, it should be identical.

So no need to panic, when in doubt erase all trees and start with a clean setup.   After a while the 66 logic will settle in and you will not have to look this up.  Feel free to experiment, you can repair this from a chroot if necessary, but even when boot parameters become impossible to boot, there is tty12 as emergency shell.  Ctrl Shift F12, log in as user (root is not allowed) look up /run/66/log/0/current.  The first reported error is what is throwing your boot process off.  If it is not enough to determine the cause increase verbosity to 4 in /etc/66/init.conf and reboot.  A more analytical error/warning will be listed.  Always read the log top-bottom, the last errors shown may be effects of the real problem, not the cause.  It is the first error that matters and should be fixed first.

