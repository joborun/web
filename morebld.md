### [[Back to the top joborun wiki page|index.md]] ###


Beyond the [[HowTo|howto]] instructions here is some information on the contents and how you can use this formula in building your own and/or contributing to joborun.

There is a PKGBUILD file that creates the joborun package in each directory.  If the package (most are) is made after Arch's pkg, there will be a file called PKGBUILD-arch, which is the PKGBUILD copy from Arch from which the pkg is created.

You can compare the two to see the differences, if any.

Why we rebuild PKGs that are the same?  Because they are built in an environment without systemd and its components.  Many though do need a direct built dependency removed.   As you see they still build without this dependency, possibly with some systemd utility also removed.

The prime format difference are our standard headers, for jobcore and jobextra, and moving the license definition, Gpg signatures, and Sha checks to the bottom of the file.  Otherwise we don't touch the format of the PKGBUILD.  This makes it much easier on upgrades to recognize changes.

During an upgrade, if you receive a copy of the new Arch PKGBUILD, say you save it as PKGBUILD-arch.new do the following:

diff -Nuar PKGBUILD-arch PKGBUILD-arch.new >>PKGBUILD

All additions/subtractions from the previous PKGBUILD will go to the bottom of the file.  - indicates what needs to be replaced and + what it is to be replaced with.  You study the changes, apply them to the file, remove this diff section from the bottom, save, and makepkg.

In some cases where the source field includes many patches, other source files, etc. we may have a file named sums.  This can be used to run a single command and figure out all the sha256sums to feed into your PKGBUILD, in the correct order.

sha256sum $(cat sums)  >>PKGBUILD

We take extra care to emphasize the importance of check sums, to eliminate the possibility of MIM attacks and backdoors going into your system.

Other differences with Arch is the removal of zstd functionality from wherever it has been added (suspiciously) in the past couple of years.  One reason is that zstd was supplied by uncle Sam's dearest partner, facebook's holding company.  Another reason is for zstd package to be created "A Google" pkg is needed.  So the two share the responsibility and avert it the same way.  We do not trust it, we trust our long term friend xz (better compression, slightly slower but we are not compressing Terrabytes of databases, slightly more ram use), or lz4, gz, bz2 etc.

A third difference, is ipv6 functionality.  We haven't found a reasonable argument of what it is we are missing with ipv6 shut off.  It is a weird not really alternative to ipv4, and few of us know how to control input/output through this "gateway" to our system.  Let's discuss this together when the community forms.  Some may have convincing arguments why we should, other than the monkey behavior "everyone else is doing it".

As you can see we take extra care to stick as close to Arch as possible with those three restrictions that define our system.  NO SYSTEMD what so ever, no zstd, no ipv6.  Everything else stays the same.


## To optimize building with makepkg ##

Read the references listed on this post about [[optimization|optimize.md]]

[[Alternatives and tips in building joborun and arch pkgs|bld-tips.md]]



