### [[Back to the top joborun wiki page|index.md]] ###


## How to keep a current and functional pacman.conf and mirrors ##

If you have let upgrades go for a while and attempting to upgrade fails (most commonly change in mirrors/host for packages and database) make sure you understand how and where to get the latest: (our source repository)

[[pacman.conf|https://git.disroot.org/joborun-pkg/jobcore/src/branch/main/pacman/pacman.conf]]

Compare with what you have or if you have never edited your own, just copy this file in /etc/

This will have the top three repositories jobcore jobextra jobcomm in this fashion


<pre>
[jobcore]
#Server = file:///var/cache/jobcore/
Include = /etc/pacman.d/mirrorlist-jobo

[jobextra]
#Server = file:///var/cache/jobextra/
Include = /etc/pacman.d/mirrorlist-jobo

[jobcomm]
#Server = file:///var/cache/jobcomm/
Include = /etc/pacman.d/mirrorlist-jobo
</pre>

The mirrorlist-jobo file can always be found on the source repository as well:

[[mirrolist-jobo|https://git.disroot.org/joborun-pkg/jobcore/src/branch/main/jobo-mirror/mirrorlist-jobo]]

which should be placed in /etc/pacman.d/mirrorlist-jobo

and currently (hopefully for a long time from July 2023) can be as simple as:

<pre>
##### Start Sourceforge mirrors #####
# Sourceforge 1
Server = http://downloads.sourceforge.net/joborun/r
</pre>

Next, to make sure you are up to date, as root, execute:

<pre>
pacman -Syy pacman jobo-mirror
</pre>

Carefully read the output of this command, run the current script that solves the problem SourceForge is creating by unable to serve filenames with a : in the middle of the name (Microsoftish filesystem that can't handle such characters ).

After this is done, from then on do a classic upgrade by executing:

<pre>
pacman -Su
</pre>

If you have further issues either send us email (web site contact joborun @ disroot org) or better, so other users can benefit, place a comment or article in reddit.com/r/joborun



