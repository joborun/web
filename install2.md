##  Install joborun using our joborun-latest.tar.xz image  ##

### [[Back to the top joborun wiki page|index.md]] ###

#### ..continued from installation [[section 1. |install.md]] ####

2  Installation using a joborun [[image|https://sourceforge.net/projects/joborun/files/images/jobo-latest.tar.xz]] and [[checksum | https://sourceforge.net/projects/joborun/files/images/joborun-img.256.sums]].  In case we have left these links outdated always check our [[downloads | https://joborun.neocities.org/download.html]] page.

Currently we only have a tarball of a base system which would need at least a kernel and a bootloader to boot.

a  Again, as usual, you must first prepare a clean partition of at least 2GiB for a base system 3GiB for one with basic graphic functionality, X + Openbox/JWM etc.  If you are unsure of this process follow the wiki from [[https://wiki.archlinux.org|Arch]] on the first steps of installation.  A common full system with multimedia tools, office-suite, browsers, games, etc. can exceed 20GB.

b  Mount the partition in a proper mounting point, for our instructions we will use /mnt but if this is used create a mounting point and replace /mnt wherever it appears in the instructions.

c  Download our latest tarball image from this page: [[ joborun.neocities.org/download.html | https://joborun.neocities.org/download.html]].

d  Download the file that states the ckecksum-256 and check the image into someplace like /tmp.
<pre>
sha256sum jobo-latest.tar.xz
cat joborun-img.256.sums
</pre>
The output of both should be the same, if not try to download again and let us know as soon as possible of the discrepancy.

<pre>
cd /mnt
sudo tar -xf /tmp/jobo-latest.tar.xz
ls -lh 
</pre>

The output of ls -lh should give you what appears as the root directory of a common linux system.

You can now chroot into the system (if you don't have the arch-chroot script in you system see the note at the bottom of this page)

<pre>
sudo arch-chroot /mnt zsh
</pre>

The following is redundant and optional, you can run joborun-setup [[script|scripts.md]] saved in /usr/local/bin and setup the installation step by step.

The /etc/locale.gen is set to UTF8 US-en, change it to the valid choice of preference, and then run locale-gen to pass the new setting to the system.  Even if you stay with US-en, run locale-gen anyway.  We have some shortcut/scripts saved in /usr/local/bin/, it is a good time to get to know them, as well as a few shortcuts saved in .zshrc (our default shell).  uuid will give you the list of partitions, their labels, and /dev/ addresses.  Run it before you prepare the fstab to make sure it is correct.  We like minimal easy to understand [[scripts|scripts.md]] that make work a little easier.

As root

Do you have a swap partition setup (let's say it is /dev/sdb8?  You should for the safety of the filesystem, beyond the insurance of the system running while exceeding its available RAM.  Say that partition is sda8, for sure, although swapon checks the validity of linux-swap filesystem before it is enabled.

<pre>swapon /dev/sdb8 </pre>

Again, you are 100% sure that is the swap partition, right?  Use the free command to verify swap is functional.

<pre>
genfstab -U / | grep -v resolv >>/etc/fstab
</pre>

Since you are on chroot the mounted /etc/resolv.conf from the host will be invalid once you boot the system, hence the exclusion of that line/entry.

Next set the password for root and user make, or add a new user if you like, but make sure you look at some helper scripts saved in /home/make that will make the installation of openbox/jwm easier, as well as make s6/66 univentful.

### To change the name of user:make to say, username33

<pre>sudo usermod -l username33 make -d /home/username33 -m </pre>


<pre>
passwd 
passwd username33    (if that is your new username)
</pre>

With the script called add.66 which will install all necessary packages to boot the system with s6/66 already setup for you.  You will only need to run 66 tools to modify the setup.  We have provided a pre-configured setup to match the one of runit so you can compare.

There is also installX (% ./installX) which will install everything we deemed necessary for a minimal window manager experience and our initial setup, expecially for openbox.  Jwm may need some attention still.  Before you run the script, see that it reads a list of packages named X.list, from which you can subtract what you know you don't need and add what you know you will like and it is available by the 3 different distributions in effect here (joborun obarun archlinux).

If you are running this from console and you left xinit inside the script it will attempt to take you to openbox as soon as the installation finishes.  If you are running from an X-terminal then since X is already running it will fail, so you may as well comment it out.

Finally, before reboot you need a kernel and a bootloader.  Assuming you are an advanced user and know how to configure grub, limine, and/or syslinux (otherwise see the obarun and archlinux wikis) choose your kernel.  Our kernels are linux-lts (5.10) and linux (5.15) which is the current lts for Arch.  Our kernels have been modified to turn off internal zstd use, and ipv6, as we do in most packages we can.  It also has the touchscreen firmware left out of the kernel to reduce the size a tiny bit.  Obarun also offers a couple of kernel sets although they tend to be outdated.  Otherwise you are on your own, but for the two kernels we carry we make it easy for the user to build on their own, and if one doesn't agree with our choices they can revert them or use the ArchLinux config which is also stored in our git.  (on building packages see the [[https://git.disroot.org/joborun/web/src/branch/main/howto.md|howto]] section of our wiki).

If you are familiar with runit we have set up a basic set of ttys dhclient and wpa_supplicant for network, ntpd for time server, and have a wide set or runscripts ready to activate as you need in /usr/lib/runit/sv as well as the standard tty definitions in /etc/runit/sv  Do not activate them though as they most likely need the software the service corresponds to, except for dbus which is a core dependency of the base system.  Dbus is not running by default, you have to make this choice for yourself.  You CAN live without it if you like.

Same exact minimalism is setup for 66.  Those [[scripts|scripts.md]] saved in /home/make are to make a tidious taks easier and complete.  They don't have to be used and each can be used in the future.

<pre>
$ pkg -Sy linux-lts grub
$ sudo grub-mkconfig -o /boot/grub/grub.config
$ sudo grub-install /dev/sda
</pre>

Adjust /dev/sda for the disk you are using to boot.  Use linux instead of linux-lts if you wand our version of linux-lts 5.15 instead of our linux-lts 5.10.  If you want something like Arch's zen kernel, you substitute linux-lts above with extra/linux-zen and headers if you need them.  Again <b>pkg</b> is a shortcut in .zshrc for <b>sudo pacman</b>.

Will get you our linux-lts 5.10 hernel (add headers if you know you need them) and install the bootloader.  Add os-prober if you want to take a chance and allow os-prober to setup multiboot for you.  For grub choices edit /etc/default/grub accordingly.

Reboot!

For our choice of Openbox and JWM setup there is a [[script|scripts.md]] installX that installs what is needed in one run.  At the end openbox starts by default and you can switch this behavior to jwm or vtwm simply by commenting/uncommenting the two exec lines of ~/.xinitrc.  You are free to add the window manager of your choice in similar fashion we haven't tried one that didn't work, vmwm included as a minimalist fun tiling/floating wm.

NOTE about arch-chroot: Basically if you have the current img of joborun linux you have access to this script and it should run on any linux/unix system.  See the article specifically for this: [[what_if_the_host_system_installing_joborun_doesnt|https://www.reddit.com/r/joborun/comments/11hyqmb/what_if_the_host_system_installing_joborun_doesnt/]] have the arch-chroot script.
