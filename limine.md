## TO BE REMOVED BY NEXT RELEASE >8.4.0

Nov 21 2024

After significant work and continuous upgrades and modifications we
have decided that limine is adopting systemd kind of logic where a distribution
should adopt to limine instead of limine being adoptable by the system.
As far as we are concerned grub is by far a much more unix like bootloader
while limine attempts linux to adopt to Microsoft standards, even for BIOS
Legacy MBR based installations.  

### DISCONTINUED 

But feel free to experiment by using the arch extra/limine pkg

__________________________

#### NOTE: this is under construction, several discrepancies, missing explanations, and 

clarifications have been reported and it will soon be edited and corrected - our apologies but documentation on limine is both scarce and contradictory when and where it is found.
Best source we see at the moment is gentoo, arch is very contradictory to its uefi /efi partitioning scheme, limine will NOT work that way.

We have tried to make it work mounting the esp flagged FAT partition to /boot/efi but have not been able to get it to boot.  Latest major release shows much renaming of things unnecesseraly, still undocumented as to the why and its effects, dropping gz support ..

This is just another piece of software that attempts to reaarrange your system just so you can make it work.  What is the problem it is solving?  Having 1 config file for booting?


### [[Back to the top joborun wiki page|index.md]] ###

*this document is a contribution by joborun community member: [Kerry Kappell (kerry@klouded.org)*](https://diaspora-fr.org/posts/10335130)


## Installing Limine on a new BIOS installation

### Additonal steps to a fresh installation:


Limine works with either a DOS or an GPT partion table. GPT is recommended.
A FAT partion is required. A modern trend is to make the FAT partion the /boot directory so make the size at least 256M or 512M.

`mkfs.fat -F 32 /dev/sdXY (for example and where X = disk letter and Y = the partiton number)`

`mkdir -p /mnt/boot/efi`

`mount /dev/sdXY /boot/efi`

`pacman -Syu linux-lts limine`

`mv /boot/efi/limine/limine.conf /boot/efi/limine/limine.conf.jobopkg`

`nano /boot/efi/limine/limine.conf `

and add:

`TIMEOUT=5`

`:Joborun Linux-LTS`

`PROTOCOL=linux`

`KERNEL_PATH=boot:///vmlinuz-linux-lts`

`CMDLINE=root=UUID=xxxxx-xxxxx ro net.ifnames=0 ipv6.disable=1 init=/usr/bin/runit-init`

`MODULE_PATH=boot:///{amd,intel}-ucode.img`

`MODULE_PATH=boot:///initramfs-linux-lts.img`


(Your UUID can be found by ‘cat /etc/fstab’ or ‘genfstab -U /’ or ls -lh /dev/disk/by-uuid )



9. limine bios-install /dev/sdX (limine-bios.sys has already been added to /boot/limine/)

#### Installing Limine on a new UEFI installation


Step 0: The following command should be run outside and inside the chroot:
mount -t efivarfs efivarfs /sys/firmware/efi/efivars

(if the host was booted from an efi system)

Steps 1 through 8 apply to a new UEFI installation as well but steps 1-5 are very familiar to UEFI setups and don’t really need instructions here.


 `pkg -S efibootmgr`

 `mount -t efivarfs efivarfs /sys/firmware/efi/efivars`

 `mkdir -p /boot/efi/EFI/BOOT`

 `cp /usr/share/limine/BOOTX64.EFI /boot/efi/EFI/BOOT/`

 `efibootmgr --create --disk /dev/sdX --part Y --loader ‘EFI\BOOT\BOOTX64.EFI’ --label ‘Limine Boot Manager’ --unicode`


where X and Y need to be replaced with your disk letter and partition number. Note that backslashes are used here instead of forward slashes. Also note that ‘/boot’ or ‘\boot’ is omitted as it is cover by the disk and partition info.

___

Of interest, both BIOS and UEFI setups can be done on the UEFI installation and now one has a dual boot setup!

___

Installing limine on an existing system with GRUB can also be done but limine does not replace or reorder the efibootmgr entries.



**References:**

- https://wiki.archlinux.org/title/Limine
- https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface
- https://wiki.archlinux.org/title/EFISTUB
- https://limine-bootloader.org/
- https://forum.garudalinux.org/t/yet-another-bonkers-bootloader-limine/34219
- https://gitee.com/input-output/limine
- https://linuxconfig.org/how-to-manage-efi-boot-manager-entries-on-linux
- https://github.com/limine-bootloader/limine/blob/trunk/test/limine.cfg
- https://www.linuxbabe.com/command-line/how-to-use-linux-efibootmgr-examples
- https://raw.githubusercontent.com/rhinstaller/efibootmgr/master/README


