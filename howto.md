### [[Back to the top joborun wiki page|index.md]] ###


You will notice that joborun has an extra directory on its root / named /src

This is where source code goes and the repository should be 0777 so user make can write into it.

If it doesn't already exist as a user create pkg  

<pre>"% mkdir -p /src/pkg</pre>

<pre>
cd /src/pkg
git clone https://git.disroot.org/joborun-pkg/jobcore
git clone https://git.disroot.org/joborun-pkg/jobextra
git clone https://git.disroot.org/joborun-pkg/jobcomm
</pre>

All of the source for joborun is in those repositories.


Shift into the name of the pkg you want to build.

You will notice various files, as usually the original PKGBUILD that "inspired" our PKGBUILD, if it came from arch it should be noted as PKGBUILD-arch and then PKGBUILD is our own version, so you can compare.  Generally the original will be from arch, or obarun PKGBUILD-oba, or AUR PKGBUILD-aur.

<pre>% cat note</pre>

if there is a note file there, it may have important information to consider before you build.

You can build any pkg in your running system but we recommend building it in a special chroot by exploding the current jobbot img found in sourceforge https://sourceforge.net/projects/joborun/files/images/

So from this point on the instructions are for using the special jobbot chroot, for any other setup adjust accordingly.

example: <pre>
mount /dev/sdz99 /mnt 
cd /mnt 
sudo tar -xf jobbot.tar.xz ./
sudo arch-chroot /mnt su make
pkg -Suy
</pre>

deps:  Is a file listing all the additional dependencies needed to build the package "on top" of the 'jobbot' minimal chroot which must always be checked before a package is built. 

 <pre>
 pacman -Qsq >/tmp/Qsq 
 diff -Nuar /src/Qsq /tmp/Qsq
 </pre>

Note: on our image jobbot tarball there should be a script named /usr/local/bin/ckchroot doing exactly this.

  If there is a difference of additionally installed packages it should 
  show in the + list.


To install additional dependencies do:

<pre>
% sudo pacman -Rnsu jobbot2
% sudo pacman -S $(cat deps)
</pre>

to restore the base chroot reverse this:

<pre>
% sudo pacman -Rnsu $(cat deps)
% sudo pacman -S jobbot2
</pre>

or make a shortcut such as depS = sudo pacman -S $(cat deps)

if you use zsh the jobbot make user has this shortcut in ~/.zshrc

key: if the source is signed, this is the GnuPgp key you need to import

Simply:   sh key


To build the package run [[/usr/local/bin/mkpkg|scripts.md]] (which is a modified way of running makepkg without risking of overexerting your machine)


The package should be produced: as pkgname-pkgver-x86_64.pkg.tar.lz 

If this is a jobcore pkg, move it to /var/cache/jobcore

If this is a jobextra pkg, move it to /var/cache/jobextra


clean:  Simply run sh clean and the directory will be left as it was when you entered it.


Summary

<pre>
cd pkgname
cat note    #  this will only exist if special instructions are needed or somthing that deviates from normal building procedure
sh key
depS
mkpkg
sudo mv pkgname*.pkg.tar.lz /var/cache/job..../
sh clean
depR
</pre>

If you want a list of all the pkg names inside each repository:

<pre>% pacman -Slq jobcore >/stc/pkg/jobcore.list</pre>

You can take the above sequence of commands (not shortcuts the complete commands) use a for pkgname in /stc/pkg/jobcore.list do ....
... and go away for the weekend, if all goes well the entire repository will be full with your own built packages.

But try to avoid such unsupervised activity.  Make a script that will build a single package but don't chainload too many of them.

## To optimize building with makepkg ##

Read the references listed on this post about [[optimization|optimize.md]]


Enjoy and report problems to reddit.com/r/joborun


PS   One thing you can do ahead of starting to build is to import all the signature keys for upstream sources.  Since all key scripts are listed in each pkg directory that employs one, when in /src/pkg/jobcore for example run:

sh */key

And all the jobcore gpg keys are imported.  Make sure there were no errors, because sometimes gnupgp key servers are slow to react and your system will not wait as long, unless you can configure it to wait longer.  Read up on the gpgme documentation.



[[More on the contents of pkg directories|morebld.md]]