### [[Back to the top joborun wiki page|index.md]] ###


## joborun packages ##

### What each one does and why ###

####   Minimal set of packages to define a basic JOBORun Linux installation  ####

#### jobcore/base ####

    Minimal set of packages to define a basic JOBORun Linux installation.  By installing base into a new partition you have a joborun bootstrap, in which you can chroot and modify your new system installation.  Pretty similar but different than Arch or Obarun, in the sense of runit pkgs being added while systemd, s6, 66 are not present.

#### jobcore/filesystem ####

    Base Joborun Linux file system.  This is the structure of the base system, files and directories, where pacman can place package files and construct the system.

#### jobcore/jobbot ####

    Minimal set of packages to maintain the building chroot for JOBORun Linux.  This is a very specific group of packages, a metapackage with the exact dependencies, in which ALL packages in joborun are built.  In each repository packages' subdirectory there exists a file named deps, which contains the additional to "jobbot" packages required to build the specific package, and after building you remove the "deps" list, and return to an absolute "jobbot" building base system.  This is how the process of building all the packages can me accelerated, simplified, and precise.  This environment is more minimal than what Arch is using, to begin with systemd and libraries don't exist, but also some building tools that are not always necessary are not there, but they can be added "only" whan they are necessary.  In addition to absolute necessities of maintaining this functional chroot, may be utilities that help you scan and download packages from repositories such as AUR or Obarun's OUR.  Like cower and package-query, which can not possibly affect the building of software in any way.

#### jobcore/jobo-setup  ####

    Post Install joborun setup script.

    There are also a pair of scripts that will add X xorg-xinit and openbox/jwm window managers or labwc for wayland.  The scripts (install remove) utilize an X.list where you can improvise and add/subtract packages to this setup to meet your needs.

    Last but not least there is a joborun-setup script that doesn't alter your installation at all no matter how many times you run it, but guides you to finalize or complete an installation if you have done so manually or without using our tarball base system.  Step by step, it tells you how to do something and provides a shell break to do it on your own, exiting back to the script's next step.

    This is the gamma (post-beta) phase of development and this list may expand a bit in the future, sometimes too fast to remind us to update this wiki page :)

    As of February of 2023 this is the status of this jobo-setup pkg.

#### jobcore/joborun-keyring  ####

    joborun PGP keyring.  This keyring package holds the single packaging team's GPG key for signing packages and repository databases.  Not yet implemented in the alpha/beta/gamma phase, and not as of high importance in a distribution intended to be source based and packages built by each sys-admin.  The initial binaries you utilize are meant to assist and accelerate the process of making your own system, from then on there is no need to download any binary, even from Arch or Obarun, you can built your own.

#### jobcore/joborun66  ####

** this is now depricated - joborun no longer supports any edition of 66 **

    for runit and 66 to coexist the 4 conflicting power functions must be separate in /usr/bin/s6 and /usr/bin/run.  This package ensures that both runit and s6/66 can coexist in the same system, they don't overlap, and while you can boot either one, you can reboot, shutdown, halt, poweroff without issues.  It may seem as a hack and not a complicated coding solution, but it works reliably.  One common function that works very well with both runit and s6 is zzz (by Void) which is incorporated into our system and works as the suspend function.  Simply execute zzz as root, later hit the power button (desktop or laptop) and the system returns to its exact previous state, with any open/running software and files, but don't push it, save your files and documents beforehand, just in case it ever fails.

#### jobcore/runit-service-scripts ####
    
    A collection of services for runit - similar to Artix Linux.  This is a bundle of service scripts without the corresponding dependencies (except for dhclient, wpa_supplicant, dbus, ntp) which are found in /usr/lib/runit/sv and can either be linked directly to /etc/runit/runsvdir/default or copied and altered in /etc/runit/sv.

    We may be offering runit as default, just to ease those inexperienced to s6/66, but we neither use it much or expect to allocate any time for developing runit's setup.  We use s6/66 ALL the time on our machines and only for the sake of experimentation and verification of other core system changes we may boot with runit.  So this area is a free for all to hack and contribute.

    The list of service scripts is long and provides also a rich base of templates to create your own such servicescripts.  By far nothing can compete or attain the stability and flexibility of configuration of 66, and by far the slow bash scripts runit's runsvdir reads are no match for s6's s6-supervise.  

    