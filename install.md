##  Installation options with joborun ##

#### [[Back to the top joborun wiki page|index.md]] ####


## There are different ways you can install joborun quickly ##


### [[-1-|install1.md]]      From a Linux system with [[pacman|install1.md]] installed ###

You can install the system with just pacman, but some of the setups stored in the image may be missed, you will have to configure things on your own.

### [[-2-|install2.md]]      By downloading our [[tarball|install2.md]] of the minimal base system ###

You get the necessary pkgs installed (documentation removed to save space) and configurations as proposed by joborun already in place.

### [[-3-|install3.md]]      Install a building environment from a Linux system with pacman installed (jobbot) ###

This last option is so you can build all the packages from source before you can boot your own joborun system.  Basically building the chroot for joborun to build all the packages in the 3 repositories, or at least the ones you need and use, and you consider to be critical and be built in safety.

#### recommended procedure ####

In case you are confused with options we recommend this following mix of installations:

A combination of methods can be used, install the system from image, boot it, try it, use the pacman -r /{chroot} method to build the initial building environment, build packages and replace them in your installation as you go.

### NOTE: Before you start building, did you know? ###

There are also options in the building environment to build packages optimized for one machine only and its specific hardware combination.  This can increase speed by 10-15% 
Our default building (as arch) is so packages work on all x86_64 architecture machines.
You can modify the gcc flags in /etc/makepkg.conf or make a local copy in ~/.makepkg.conf and follow these instructions: [[gentoo optimization | https://wiki.gentoo.org/wiki/GCC_optimization ]]

