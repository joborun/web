### [[Back to the top joborun wiki page|index.md]] ###


Note: July 2023
What is explained below is also found on a pair of scripts in /usr/local/bin
named  66-user-up/down to do pretty much the same as the procedure explained 
here.

First create the tree as root so s6 will look for user services for this particular user.

`# 66-tree -nE boot-user`

then enable the following services on say a user named jo (replace jo with your own) but to enable those services, you must have the service files, which we can't assume you do unless you install the boot-user@-66serv package. '# pacman -S boot-user@-66serv`

`# 66-enable -t boot-user -FS scandir@jo setenv@jo svscan@jo mount-run@jo dbus ....`

dbus is included since it is most likely the reason you created the boot-user module so you can run user level services as dbus-user, you can add consolekit to the tree if it is needed (install consolekit2 and consolekit-66serv pkgs first).

Now as user:

`% 66-tree -nEc jo`

`% 66-enable -FS dbus-session@jo xdg-user-dirs`


To see if it all works run

`# 66-intree -zg boot-user`

`% 66-intree -zg jo`


`#` indicates a root command

`%` indicates a user command

`# 66-all -t boot-user up`

`% 66-all -t jo up`

If it didn't work immediately it will work on next boot or next X session. 
Basically if you did this on a terminal, exit X, exit user, re-login.
The difference with doing the same, as Obarun says, with enabling and 
configuring the boot-user@<user-name> module is that you have to edit 
.xinitrc if you are using consolekit. For example, to start a wm like 
jwm with consolekit the .xinitrc should say


`exec ck-launch-session jwm`

with ck running instead of ...


`exec jwm`

.. without consolekit




This also means that the X session should restart for it to be in effect, otherwise ck is useless if you are doing this from a terminal withon the x-server session.

If it works /run/66/ will be populated correctly for user 1000 or whatever the id for the user is, s6 scans those directories for what service files to supervise. This is what the scandir initiates.

I encourage everyone to read the 66 documentation, it is not that much and in most cases it is very easy to understand if you keep experimenting and trying things live. Just don't mess with the boot@ module live.

`# 66.setup`

will get you back to the default initial joborun setup (which now includes the above procedure but leaves boot-user tree not enabled/running) if you have messed up.  You must enable the boot-user tree and the user's tree base.  To enable an inactive tree of services use the 66-tree command:

`# 66-tree -E boot-user`

`% 66-all base up`


So go ahead and experiment and read the 66 documentation and wiki, especially the introduction and FAQ

This how-to set of instructions can also be found in the Joborun-Wiki https://git.disroot.org/joborun/web/src/branch/main/66user.md


